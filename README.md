## Daftar Isi
- [Tujuan](#tujuan)
- [Ukuran Pencapaian](#ukuran-pencapaian)
- [Contoh Sederhana REST-API](#contoh-sederhana-rest-api)

## Tujuan
1. Bagaimana menyediakan web-service REST-API dengan Spring Boot
2. Mengenal Maven sebagai build-tool

## Ukuran Pencapaian
1. Dapat membuat web-service REST-API dengan menggunakan Spring Boot
2. Menyesuaikan format request dan response API sesuai kebutuhan
3. Mengerti cara membentuk struktur project Java sesuai ketentuan Maven
4. Mengerti apa yang terkandung di dalam file pom.xml

## Contoh Sederhana REST-API
Baca:
1. [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service)
2. [Building Java Projects with Maven](https://spring.io/guides/gs/maven)